﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DOITOEMApi.Models
{
    //public class RootObject
    //{
    //   public RegData regData { get; set; }
    //}
    public class pVal
    {
        // // "pVal":[{"BitOrCode":"","Val":"50","UCL":"-30","LCL":"60","OPR":"/","Factor":"2"}]}
        public string BitOrCode { get; set; }
        public string Val { get; set; }
        public string LCL { get; set; }
        public string UCL { get; set; }
        public string OPR { get; set; }
        public string Factor { get; set; }
     

    }
    //OEMID	OEMDesc	MasterID	MasterDesc	PlcID	PlcDesc	RegisterID	Category	Sub-Category	pName	pUOM	pType	BitOrCode	Val	UCL	LCL	OPR	Factor																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														
   // {"UniqueId":"1234-1001-2001-41001","OEMID":"1234","OEMDesc":"Nichrome","MasterID":"1001","MasterDesc":"LiquidFilling",
    public class RootObject
    {
        public string UniqueId { get; set; }
        public int OEMID { get; set; }
        public string OEMDesc { get; set; }
        public int  MasterID{ get; set; }
        public string MasterDesc { get; set; }
        public int PlcID { get; set; }
        public string PlcDesc { get; set; }
        public string RegisterID { get; set; }
        //public string BitOrCode { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string pName { get; set; }
        public string pUOM { get; set; }
        public string pType { get; set; }
        public List<pVal> pVal { get; set; }

      
    }

   

}