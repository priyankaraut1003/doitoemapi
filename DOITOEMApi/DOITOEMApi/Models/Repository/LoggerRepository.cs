﻿ using DOITOEMApi.BusinessLogic;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace DOITOEMApi.Models.Repository
{
    public class LoggerRepository : IOTHubRepository
    {
        Int32 batchSize = Convert.ToInt32( ConfigurationManager.AppSettings["batchSize"]);
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        IOTBL iOTBL = new IOTBL();
        List<RootObject> machinePropertiesList;
        public void CheckLoggerData()
        {
            try
            {

                using (var dbconn = new IoT_dataEntities1())
                {
                    var iotJsonDataList = dbconn.usp_GetIOTJsonDataByBatchNew(batchSize).ToList();

                    if (iotJsonDataList != null)
                    {
                        foreach (var iotJsonData in iotJsonDataList)
                        {
                            #region we need to handle concatenate jsondata 1 to 5 columns/validate and  remove first three repeated register
                            // read jsondata1 columns store value first 3 register position another variables 
                            ///check the if json string exits in column 2 to 5 
                            ///if string exits any of column then check if those 3 register and remove register value
                            ///concatenate string
                            ///
                            JObject jsonStrFinal = iOTBL.ValidateLoggerData(iotJsonData);
                            #endregion
                            Boolean isJsonValid = iOTBL.IsValidJson(jsonStrFinal.ToString());
                            iotJsonData.JsonData1 = jsonStrFinal.ToString();
                            if (isJsonValid)
                            {
                                bool result = SaveDataIntoFinalDT(iotJsonData);
                                if (result)
                                {
                                    log.Info("Finished Successfully for - " + iotJsonData.ID + " @" + DateTime.Now);
                                    using (var dbconn1 = new IoT_dataEntities1())
                                    {
                                        dbconn1.usp_Set_DataIsRead_to_1(iotJsonData.ID);
                                    }
                                }
                                else
                                {
                                    log.Info("Processing Not Successful for - " + iotJsonData.ID + " @" + DateTime.Now);
                                }
                            }
                            else
                            {
                                log.Error("Json string is not valid");
                            }

                        }

                    }
                    else
                    {
                        log.Info("No readable record found in Logger Database " + " @" + DateTime.Now);
                    }
                }

                
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private bool SaveDataIntoFinalDTOneString(int plcRunId, List<RootObject> machinePropertiesList, Dictionary<string, string> deserializeJsonObj)
        {
            bool result = false;
            Hashtable hashtable = new Hashtable();
            DataTable tempDT = new DataTable();
            tempDT.Columns.Add("PlcRunID", typeof(string));
            tempDT.Columns.Add("PLCRegisterID", typeof(string));
            tempDT.Columns.Add("BitCodePosition", typeof(string));
            tempDT.Columns.Add("ParameterCategory", typeof(string));
            tempDT.Columns.Add("ParameterUnit", typeof(string));
            tempDT.Columns.Add("ParameterName", typeof(string));
            tempDT.Columns.Add("ParameterValueDisplay", typeof(string));
            tempDT.Columns.Add("ParameterValueActual", typeof(string));
            tempDT.Columns.Add("OKToUpload", typeof(bool));

            try
            {
                foreach (KeyValuePair<string, string> sRegister in deserializeJsonObj)
                {
                    string regId = sRegister.Key;
                    string regActualVal = sRegister.Value;
                    DataRow dr; double dTemp = 0;
                    dr = tempDT.NewRow();

                    RootObject regRecord = machinePropertiesList.Where(s => s.RegisterID == regId).Select(p => p).FirstOrDefault();
                    if (regRecord != null)
                    {

                        dr["PlcRunID"] = plcRunId;
                        dr["PLCRegisterID"] = regId;
                        dr["ParameterCategory"] = regRecord.Category;
                        //add sub-category after sql table 
                        dr["ParameterUnit"] = regRecord.pUOM;
                        dr["ParameterName"] = regRecord.pName;
                        dr["OKToUpload"] = true;

                        string pTypeValue = regRecord.pType;
                        string sBitOrCode = regRecord.pVal.Select(s => s.BitOrCode).FirstOrDefault();
                        string sVal = regRecord.pVal.Select(s => s.Val).FirstOrDefault();
                        string sUcl = regRecord.pVal.Select(s => s.UCL).FirstOrDefault();
                        string sLcl = regRecord.pVal.Select(s => s.LCL).FirstOrDefault();
                        string sOperator = regRecord.pVal.Select(s => s.OPR).FirstOrDefault();
                        string sFactor = regRecord.pVal.Select(s => s.Factor).FirstOrDefault();


                        switch (pTypeValue.ToUpper())
                        {
                            case "STR":
                                dr["ParameterValueActual"] = regActualVal;
                                dr["ParameterValueDisplay"] = sVal;
                                tempDT.Rows.Add(dr);
                                break;
                            case "INT":

                                //ID MasterMappingID RegisterProperties 
                                //1   NULL    { "UniqueId":"1234-1001-2001-41001","OEMID":"1234","OEMDesc":"Nichrome","MasterID":"1001","MasterDesc":"LiquidFilling","PlcID":"2001","PlcDesc":"LH","RegisterID":"41001","Category":"IDENTITY","Sub-Category":"MyValue","pName":"LOGGER ID","pUOM":"MACID",
                                //"pType":"STR","pVal":[{"BitOrCode":"","Val":"00:1B:44:11:3A:B7","UCL":"","LCL":"","OPR":"","Factor":""}]}
                                //2	NULL	{"UniqueId":"1234-1001-2001-41002","OEMID":"1234","OEMDesc":"Nichrome","MasterID":"1001","MasterDesc":"LiquidFilling","PlcID":"2001","PlcDesc":"LH","RegisterID":"41002","Category":"IDENTITY","Sub-Category":"MyValue","pName":"OEM ID","pUOM":"unit",
                                //"pType":"INT","pVal":[{"BitOrCode":"","Val":"1234","UCL":"","LCL":"","OPR":"","Factor":""}]}	2019-12-17 03:53:00	NULL NULL    NULL
                                //5	NULL	{"UniqueId":"1234-1001-2001-41005","OEMID":"1234","OEMDesc":"Nichrome","MasterID":"1001","MasterDesc":"LiquidFilling","PlcID":"2001","PlcDesc":"LH","RegisterID":"41005","Category":"Web Servo","Sub-Category":"MyValue","pName":"Temp","pUOM":"degC",
                                //"pType":"FLT","pVal":[{"BitOrCode":"","Val":"50","UCL":"-30","LCL":"60","OPR":"/","Factor":"2"}]}	2019-12-17 03:53:00	NULL NULL    NULL

                                string valTemp = "Default#" + sVal;
                                if (!String.IsNullOrEmpty(sUcl))
                                {
                                    valTemp = valTemp + " UCL# " + sUcl;
                                }
                                if (!String.IsNullOrEmpty(sLcl))
                                {
                                    valTemp = valTemp + " LCL# " + sLcl;
                                }
                                dr["ParameterValueDisplay"] = valTemp;

                                if (!String.IsNullOrEmpty(sFactor) && (!String.IsNullOrEmpty(sOperator)))
                                {

                                    double dActVal = Convert.ToDouble(regActualVal);
                                    double dFactor = Convert.ToDouble(sFactor);
                                    if (dFactor != 0)
                                    {
                                        switch (sOperator)
                                        {
                                            case "+":
                                                dTemp = dActVal + dFactor;
                                                break;
                                            case "-":
                                                dTemp = dActVal - dFactor;
                                                break;
                                            case "*":
                                                dTemp = dActVal * dFactor;
                                                break;
                                            case "/":
                                                dTemp = dActVal / dFactor;
                                                break;
                                            case "%":
                                                dTemp = dActVal % dFactor;
                                                break;
                                            default:
                                                dTemp = dActVal;
                                                break;

                                        }
                                        dr["ParameterValueActual"] = Convert.ToInt32(dTemp);
                                    }
                                    else
                                    {
                                        dr["ParameterValueActual"] = regActualVal;
                                    }
                                }
                                else
                                {
                                    dr["ParameterValueActual"] = regActualVal;
                                }

                                tempDT.Rows.Add(dr);
                                break;
                            case "FLT":

                                valTemp = "Default#" + sVal;
                                if (!String.IsNullOrEmpty(sUcl))
                                {
                                    valTemp = valTemp + " UCL# " + sUcl;
                                }
                                if (!String.IsNullOrEmpty(sLcl))
                                {
                                    valTemp = valTemp + " LCL# " + sLcl;
                                }

                                dr["ParameterValueDisplay"] = valTemp;

                                if (!String.IsNullOrEmpty(sFactor) && (!String.IsNullOrEmpty(sOperator)))
                                {

                                    double dActVal = Convert.ToDouble(regActualVal);
                                    double dFactor = Convert.ToDouble(sFactor);
                                    if (dFactor != 0)
                                    {
                                        switch (sOperator)
                                        {
                                            case "+":
                                                dTemp = dActVal + dFactor;
                                                break;
                                            case "-":
                                                dTemp = dActVal - dFactor;
                                                break;
                                            case "*":
                                                dTemp = dActVal * dFactor;
                                                break;
                                            case "/":
                                                dTemp = dActVal / dFactor;
                                                break;
                                            case "%":
                                                dTemp = dActVal % dFactor;
                                                break;
                                            default:
                                                dTemp = dActVal;
                                                break;

                                        }
                                        dr["ParameterValueActual"] = dTemp;
                                    }
                                    else
                                    {
                                        dr["ParameterValueActual"] = regActualVal;
                                    }
                                }
                                else
                                {
                                    dr["ParameterValueActual"] = regActualVal;
                                }
                                //dr["ParameterValueDisplay"] = "Default#" + regRecord.pVal.Select(s => s.Val).FirstOrDefault() + " UCL# " + regRecord.pVal.Select(s => s.UCL).FirstOrDefault() + " LCL# " + regRecord.pVal.Select(s => s.LCL).FirstOrDefault();

                                tempDT.Rows.Add(dr);
                                break;

                            case "CODE":
                                //{..."pType":"CODE","pVal":[{"BitOrCode":"0","Val":"SCHNEIDER","UCL":"","LCL":"","OPR":"","Factor":""},{"BitOrCode":"1","Val":"DELTA","UCL":"","LCL":"","OPR":"","Factor":""},{},{}]}
                                dr["BitCodePosition"] = regActualVal;
                                var codeRec = regRecord.pVal.Where(s => s.BitOrCode == regActualVal).Select(p => p.Val).FirstOrDefault();
                                if (codeRec != null)
                                    dr["ParameterValueActual"] = codeRec;
                                else
                                    dr["OKToUpload"] = false;
                                tempDT.Rows.Add(dr);
                                break;
                            case "BIT":
                                //{ ...,"pType":"BIT","pVal":[{"BitOrCode":"0","Val":"POWER STATUS","UCL":"OFF","LCL":"ON","OPR":"","Factor":""},{"BitOrCode":"1","Val":"SWITCH","UCL":"OFF","LCL":"ON","OPR":"","Factor":""}]}
                                int bitvalue = Convert.ToInt32(regActualVal);
                                string binary = Convert.ToString(bitvalue, 2);
                                int size = binary.Length;
                                char[] bitArray = binary.ToCharArray();
                                int bitPst = 0;
                                // { "RegData":{ "ID":41307,"pCat":"ON LINE ","pName":"CIP STATUS","pType":"BIT","pVal":[{"bPos":0,"bName":"REQUEST","b0":"NO","b1":"YES"},{"bPos":1,"bName":"PENDING","b0":"NO","b1":"YES"},{"bPos":2,"bName":"START","b0":"NO","b1":"YES"},{"bPos":3,"bName":"IN PROCESS","b0":"NO","b1":"YES"},{"bPos":4,"bName":"COMPLETE","b0":"NO","b1":"YES"}]}}
                                //{"RegData":{"ID":41305,"pCat":"ON LINE ","pName":"MACHINE STATUS","pType":"BIT","pVal":[{"bPos":0,"bName":"POWER STATUS","b0":"OFF","b1":"ON"},{"bPos":1,"bName":"READY STATUS","b0":"OFF","b1":"ON"},{"bPos":2,"bName":"PRODUCTION STATUS","b0":"OFF","b1":"ON"}]}}
                                foreach (char onebit in bitArray.Reverse())
                                {
                                    var bitRecord = regRecord.pVal.Where(s => Convert.ToInt32(s.BitOrCode) == bitPst).FirstOrDefault();
                                    if (bitRecord == null)
                                    {
                                        dr["ParameterValueActual"] = "bit pos # " + bitPst + " value not found ";
                                        dr["OKToUpload"] = false;
                                    }
                                    else if (onebit == '0')
                                    {
                                        dr["ParameterValueActual"] = bitRecord.Val + " " + bitRecord.UCL;
                                    }
                                    else
                                    {
                                        dr["ParameterValueActual"] = bitRecord.Val + " " + bitRecord.LCL;
                                    }
                                    tempDT.ImportRow(dr);
                                    bitPst++;
                                }
                                //dr["ParameterName"] = "Bit Data Not Processed";
                                break;

                            default:

                                break;
                        }

                        //  [PlcRunID],[PLCRegisterID],[BitCodePosition],[ParameterCategory],[ParameterUnit]
                        //  ,[ParameterName],[ParameterValueDisplay],[ParameterValueActual]

                        //@iPlcRunID,@sPLCRegisterID,@iBitCodePosition,@sParameterCategory,@sParameterUnit,@sParameterName,@sParameterValueDisplay,@sParameterValueActual)

                    }
                    else
                    {
                        dr["PlcRunID"] = plcRunId;
                        dr["PLCRegisterID"] = regId;
                        dr["ParameterName"] = "Master Data Not Found";
                        dr["ParameterValueActual"] = regActualVal;
                        dr["OKToUpload"] = false;
                        tempDT.Rows.Add(dr);
                    }

                }
                var srNo = tempDT.AsEnumerable().Where(s => s.Field<string>("ParameterName") == "SO NO").Select(P => P.Field<string>("ParameterValueActual")).FirstOrDefault();
                var parameter = new SqlParameter("@tblcode", SqlDbType.Structured);
                parameter.Value = tempDT;
                parameter.TypeName = "dbo.tempDT";
                //parameter.TypeName = "UserType";

                using (var dbconn = new IoT_dataEntities1())
                {
                    // dbconn.usp_Save_FinalData2();
                    int? rtvalue = dbconn.Database.ExecuteSqlCommand("exec dbo.usp_Save_FinalData @tblcode", parameter);
                  //  int? resVal = dbconn.usp_Set_SO_NO(Convert.ToInt32(srNo), plcRunId);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                result = false;
            }
            return result;

        }
        private bool SaveDataIntoFinalDT(usp_GetIOTJsonDataByBatchNew_Result iotJsonData)
        {
            #region Declaration
            bool result = false;
            IOTBL iOTBL = new IOTBL();

            #endregion
            try
            {
                log.Info("Log is StART");

                String jsonDataStr = iotJsonData.JsonData1;
                // String jsonDataStr = iotJsonData.Select(s => s).FirstOrDefault();
                var deserializeJsonObj = new JavaScriptSerializer().Deserialize<Dictionary<String, String>>(jsonDataStr);
                #region jsonString 1 to 10 reserve for Register 1 to 5 get information and 6 to 10 are spare Register Value
                //OEMID / MasterID / PlcID / MachineID / CustomerID / LoggerID / Spare /spare /Spare / spare

                var oemIDElement = deserializeJsonObj.ElementAt(0);
                var masterIdElement = deserializeJsonObj.ElementAt(1);
                var plcIdElement = deserializeJsonObj.ElementAt(2);

                //var loggerElement = deserializeJsonObj.ElementAt(0);
                //var endCustomerIdElement = deserializeJsonObj.ElementAt(4);
                //use for testing

                //string sUniqueKey = oemIDElement.Value +"-"+masterIdElement.Value +"-"+ plcIdElement.Value+"%";  //"1234-1001-2001%";
                string sUniqueKey = "1234-1001-2001%";
                #endregion
                machinePropertiesList = iOTBL.usp_GetMstRegisterDataList(sUniqueKey);

                //After live data start use following line
                //machinePropertiesList = iOTBL.usp_GetMstRegisterDataList(Convert.ToInt32( machinelineIdElement.Value));


                if (machinePropertiesList != null && machinePropertiesList.Count > 1)
                {

                    //machinePropertiesList is mstRegister Data List
                    //deserializeJsonObj is one record from IOT_JSON_DATA_V2(data sent by logger)
                    int plcRunID = iOTBL.CreatePlcRunId(iotJsonData.ID);
                    if (plcRunID != 0)
                    {
                        result = SaveDataIntoFinalDTOneString(plcRunID, machinePropertiesList, deserializeJsonObj);
                    }

                }
                else
                {
                    log.Error("Json String is Not Valid");//json string not valid
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString());
                result = false;
                // throw;
            }

            return result;
        }


    }
}