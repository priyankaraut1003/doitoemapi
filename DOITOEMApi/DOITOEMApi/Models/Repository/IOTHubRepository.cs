﻿
using DOITOEMApi.BusinessLogic;
using DOITOEMApi.Models.IRepository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

namespace DOITOEMApi.Models.Repository
{
    public class IOTHubRepository : IIOTHubRepository
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Repository _repository = new Repository();
        IOTBL iOTBL = new IOTBL();
        List<RootObject> machinePropertiesList;
        SQLDataAccess sqlDataAccess;
        public static DataTable nichromeCustDT;
        public static DataTable nichromeProdDT;
        public JObject LiveToArchiveData()
        {
            int oemId = 1234 ;
            int customerId = 1196;
            int days = 7;
            try
            {
                //using (var dbconn =new IoT_dataEntities1)
                //{
                //   dbconn.usp_LiveToArchiveData()
                //}
                return new JObject
                {
                    {"statusCode", 200},
                    {"message", "Success"},
                    {"data", null},
                    {"status", true}
                };
            }
            catch (Exception ex)
            {

                log.Error(ex.Message);
                return new JObject
                {
                    {"statusCode", 404},
                    {"error", ex.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        #region CSV file Read
        /// <summary>
        /// this method for read csv Data into Datatable and sorted row using unique id and bit n code
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>DataTable</returns>
        public DataTable ReadCsvData(String filePath)
        {
            DataTable csvDT = new DataTable();
            csvDT = iOTBL.ReadCsvFileUsingUniqueID(filePath);

             DataView dv = new DataView(csvDT);
            dv.Sort = "UNQID ,BitOrCode ASC";

            csvDT = dv.ToTable();

            return csvDT;
        }
        /// <summary>
        /// this method for create one jsonString given DataTable
        /// </summary>
        /// <param name="csvTable"></param>
        /// <returns>Json Serialise String</returns>
        public static string CsvToJsonUsingNewtonSoft(DataTable csvTable)
        {
            StringWriter sw = new StringWriter();
            JsonTextWriter writer = new JsonTextWriter(sw);
            int rowsCnt = csvTable.Rows.Count;
            List<String> colHeaderList = new List<string>();  //get all coloum name
            foreach (DataColumn iemColName in csvTable.Columns)
            {
                colHeaderList.Add(iemColName.ColumnName);
            }
            int pTypePos = colHeaderList.FindIndex(a => a.Contains("pType"));
            int totalCols = colHeaderList.Count;
            int valCols = totalCols - (pTypePos + 1);

            // {
            writer.WriteStartObject();

            for (int i = 0; i <= pTypePos; i++)
            {
                //dctFinalList.Add(colHeaderList[i], csvTable.Rows[0][i]);
                // "name" : "Jerry"
                writer.WritePropertyName(colHeaderList[i]);
                writer.WriteValue(csvTable.Rows[0][i]);
            }
            //"pVal":[{"iVal":10,"iName":"PY"},{"iVal":14,"iName":"13-14"},{"iVal":16,"iName":"15-16"}]
            writer.WritePropertyName("pVal");
            writer.WriteStartArray();

            for (int i = 0; i < rowsCnt; i++)
            {
                // {
                writer.WriteStartObject();
                for (int j = 0; j < valCols; j++)
                {
                    writer.WritePropertyName(colHeaderList[j + (pTypePos + 1)]);
                    writer.WriteValue(csvTable.Rows[i][j + (pTypePos + 1)]);

                    //OEMID,OEMDesc,MasterID,MasterDesc,PlcID,PlcDesc,RegisterID,Category,SubCategory,pName,pUOM,pType,BitOrCode,Val,UCL,LCL,OPR, Factor;
                    
                }
                // }
                writer.WriteEndObject();
            }
            // ]
            writer.WriteEndArray();
           
            // }
            writer.WriteEndObject();

            return sw.ToString();
        }


        /// <summary>
        /// This method write for given csv file path .
        /// Read All Data into DataTable and create into json string
        /// </summary>
        /// <param name="filePath"></param>
        public void readCsvDataAndCreateJsonString(string filePath)
        {
            DataTable csvDT = ReadCsvData(filePath);
            DataTable jsonDT = new DataTable();
            //jsonDT.Columns.Add("RegisterID", typeof(int));
            //jsonDT.Columns.Add("PlcID", typeof(int));
            jsonDT.Columns.Add("RegisterProperties", typeof(string));

            for (int i = 0; i < csvDT.Rows.Count;)
            {
                string strUK = csvDT.Rows[i]["UNQID"].ToString();
                DataTable tblFiltered = csvDT.AsEnumerable()
                                     .Where(row => row.Field<string>("UNQID") == strUK)
                                     .CopyToDataTable();

                String serialiseStr1 = CsvToJsonUsingNewtonSoft(tblFiltered);
                JObject rss = JObject.Parse(serialiseStr1);
                string registerID = (string)rss["RegisterID"];
                string plcID = (string)rss["PlcID"];
                DataRow dr = jsonDT.NewRow();
               // dr["RegisterID"] = (string)rss["RegisterID"];
               // dr["PlcID"] = (string)rss["PlcID"];
                dr["RegisterProperties"] = serialiseStr1;
                jsonDT.Rows.Add(dr);

                //iOTBL.createxml(jsonDT);

                int indexCnt = tblFiltered.Rows.Count;
                i = i + indexCnt;
            }

            try
            {

                var parameter = new SqlParameter("@tblcode", SqlDbType.Structured);
                parameter.Value = jsonDT;
                parameter.TypeName = "dbo.[tempMstRegsterDT]";

                using (var dbconn = new IoT_dataEntities1())
                {
                    
                    dbconn.Database.ExecuteSqlCommand("exec dbo.usp_Save_mstRegisterData @tblcode", parameter);
                    //result = true;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                //throw;
            }




            #region this method for pass DataTable get dictonary and serialise into string
            //DataTable strDT = csvDT.AsEnumerable().Where(r => r.Field<string>("RegisterID") == "941001").CopyToDataTable();
           // String serialiseStr = CsvToJsonUsingNewtonSoft(strDT);
            //Dictionary<String, object> dctListS = CreateDictonaryListJson(strDT);
            //string jsonS = new  JavaScriptSerializer().Serialize(dctListS);
            #endregion
        }
        #endregion

        #region this Method for save Data
        /// <summary>
        /// this method for read IOT_JSON_DATA_V2 table data and insert into FinalMachineData
        /// </summary>
        public JObject CheckLoggerData()
         {
            
            Hashtable hInputPara = new Hashtable();
            Int32 batchSize = Convert.ToInt32( ConfigurationManager.AppSettings["batchSize"]);
            try
            {
                // 1001 is machineLineId  

                using (var dbconn = new IoT_dataEntities1())
                {
                    //var parameter = new SqlParameter("@iBatchSize", SqlDbType.Structured);
                    //parameter.Value = 40;
                    //parameter.TypeName = "@iBatchSize";
                    //var result=  dbconn.Database.ExecuteSqlCommand("exec dbo.usp_GetIOTJsonDataByBatch ", 70);

                    var iotJsonDataList = dbconn.usp_GetIOTJsonDataByBatchNew(batchSize).ToList();

                    if (iotJsonDataList.Select(s => s.ID).FirstOrDefault()!=0)
                    {
                        #region 
                        sqlDataAccess = new SQLDataAccess();
                        nichromeCustDT = new DataTable();
                        nichromeProdDT = new DataTable();
                        nichromeCustDT = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_DOIT_GetNichromeCustomerTable");
                        nichromeProdDT = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_DOIT_GetNichromeProductTable");
                        #endregion
                        foreach (var iotJsonData in iotJsonDataList)
                        {
                            #region we need to handle concatenate jsondata 1 to 5 columns/validate and  remove first three repeated register
                            // read jsondata1 columns store value first 3 register position another variables 
                            ///check the if json string exits in column 2 to 5 
                            ///if string exits any of column then check if those 3 register and remove register value
                            ///concatenate string
                            ///
                            JObject jsonStrFinal = JObject.Parse(iotJsonData.JsonData1) ;

                            if (!String.IsNullOrEmpty(iotJsonData.JsonData1))
                            {
                                 jsonStrFinal = JObject.Parse(iotJsonData.JsonData1);

                                if (!String.IsNullOrEmpty(iotJsonData.JsonData2))
                                {
                                    JObject jsonStrCol2 = JObject.Parse(iotJsonData.JsonData2);
                                    jsonStrFinal.Merge(jsonStrCol2);
                                    
                                }
                                if (!String.IsNullOrEmpty(iotJsonData.JsonData3))
                                {
                                    JObject jsonStrCol3 = JObject.Parse(iotJsonData.JsonData2);
                                    jsonStrFinal.Merge(jsonStrCol3);

                                }
                                if (!String.IsNullOrEmpty(iotJsonData.JsonData4))     
                                {
                                    JObject jsonStrCol4 = JObject.Parse(iotJsonData.JsonData4);
                                    jsonStrFinal.Merge(jsonStrCol4);
                                }
                                if (!String.IsNullOrEmpty(iotJsonData.JsonData5))
                                {
                                    JObject jsonStrCol5 = JObject.Parse(iotJsonData.JsonData5);
                                    jsonStrFinal.Merge(jsonStrCol5);
                                }
                            }
                            #endregion
                            Boolean isJsonValid = iOTBL.IsValidJson(jsonStrFinal.ToString());
                            iotJsonData.JsonData1 = jsonStrFinal.ToString();
                            if (isJsonValid) 
                            {
                                log.Info("Started for - " + iotJsonData.ID + " @" + DateTime.Now);
                                bool result = iOTBL.SaveDataIntoFinalDT(iotJsonData);
                                if (result)
                                {
                                    log.Info("Finished Successfully for - " + iotJsonData.ID + " @" + DateTime.Now);
                                   
                                }
                                else
                                {
                                    log.Info("Processing Not Successful for - " + iotJsonData.ID + " @" + DateTime.Now);
                                }
                                using (var dbconn1 = new IoT_dataEntities1())
                                {
                                    dbconn1.usp_Set_DataIsRead_to_1(iotJsonData.ID);
                                }
                            }
                            else
                            {
                                log.Error("Json string is not valid");
                            }
                            
                        }
                        
                    }
                    else
                    {
                        log.Error("No readable record found in Logger Database " + " @" + DateTime.Now);
                    }

                    return new JObject
                    {
                    {"statusCode", 200},
                    {"message", "Success"},
                    {"data", null},
                    {"status", true}
                    };
               
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return new JObject
                {
                    {"statusCode", 404},
                    {"error", ex.Message},
                    {"data", null},
                    {"status", false}
                };

            }

          
        }
        public int CreatePlcRunId(int recId)
        {
            
            int plcRunId = 0;
            try
            {
                using (var dbconn = new IoT_dataEntities1())
                {
                    var plcRunIdObj = dbconn.usp_CreateAndGetPlcRunID();
                    plcRunId = Convert.ToInt32(plcRunIdObj.Select(s => s.Value).FirstOrDefault());
                }
            }
            catch(Exception ex)
            {
                log.Error(ex.Message);
            }
            return plcRunId;
        }
        public Dictionary<string, string> GetCustomerData(Int32 custNo)
        {
            Dictionary<string, string> custDct = new Dictionary<string, string>();
            try
            {

                string customerCode = 'C' + custNo.ToString().PadLeft(6, '0');

                sqlDataAccess = new SQLDataAccess();
                Hashtable inputParams = new Hashtable();
                inputParams.Add("@sCustomerCode", customerCode);

                var rows = nichromeCustDT.AsEnumerable().Where(w => w.Field<string>("Customer_Code") == customerCode);

                var custNameDT = rows.Any() ? rows.CopyToDataTable() : nichromeCustDT.Clone();
                // var custNameDT = sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_GetCustomerNameUsingCustomerNO]", inputParams);

                if (custNameDT.Rows.Count > 0)
                {
                    custDct.Add("CUSTOMERNAME", custNameDT.Rows[0]["CustomerName"].ToString());
                    custDct.Add("CUSTOMERCODE", custNameDT.Rows[0]["Customer_Code"].ToString());
                    custDct.Add("REGION", custNameDT.Rows[0]["Region"].ToString());
                    custDct.Add("PERSON", custNameDT.Rows[0]["Person"].ToString());
                }
                else
                {
                    custDct.Add("CUSTOMERNAME", "NOT FOUND");
                    custDct.Add("CUSTOMERCODE", customerCode);
                    custDct.Add("REGION", "NOT FOUND");
                    custDct.Add("PERSON", "NOT FOUND");
                }
            }
            catch(Exception ex)
            {
                log.Error(ex.Message.ToString());
            }
            return custDct;
        }

        public Dictionary<string, string> GetProductDetails(Int32 productId)
        {
            //sqlDataAccess = new SQLDataAccess();
            Dictionary<string, string> productDct = new Dictionary<string, string>();
            //Hashtable inputParams = new Hashtable();
            //inputParams.Add("@iProductID", productId);
            try
            {
                var rows = nichromeProdDT.AsEnumerable()
                               .Where(row => row.Field<int>("ProductID") == productId);
                var productDT = rows.Any() ? rows.CopyToDataTable() : nichromeProdDT.Clone();
                
                //var productDT = sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_GetProductDetails]", inputParams);
                if (productDT.Rows.Count > 0)
                {
                    productDct.Add("ProductCategory", productDT.Rows[0]["ProductCategory"].ToString());
                    productDct.Add("ProductSegment", productDT.Rows[0]["ProductSegment"].ToString());
                    productDct.Add("ProductPackedID", productDT.Rows[0]["ProductPackedID"].ToString());
                    productDct.Add("ProductPackedDesc", productDT.Rows[0]["ProductPackedDesc"].ToString());
                }
                else
                {
                    productDct.Add("ProductCategory", "NOT FOUND");
                    productDct.Add("ProductSegment", "NOT FOUND");
                    productDct.Add("ProductPackedID", "NOT FOUND");
                    productDct.Add("ProductPackedDesc", "NOT FOUND");
                }
            }
            catch(Exception ex)
            {

            }
            return productDct;
        }
        #endregion

        public bool SaveDataIntoFinalDT(int plcRunId, List<RootObject> machinePropertiesList, Dictionary<string, string> deserializeJsonObj,Int32 recId,string recIDDT)
        {
            bool result = false;
            Hashtable hashtable = new Hashtable();
            DataTable tempDT = new DataTable();
            tempDT.Columns.Add("PlcRunID", typeof(int));
            tempDT.Columns.Add("PLCRegisterID", typeof(string));
            tempDT.Columns.Add("BitCodePosition", typeof(string));
            tempDT.Columns.Add("ParameterCategory", typeof(string));
            tempDT.Columns.Add("ParameterUnit", typeof(string));
            tempDT.Columns.Add("ParameterName", typeof(string));
            tempDT.Columns.Add("ParameterValueDisplay", typeof(string));
            tempDT.Columns.Add("ParameterValueActual", typeof(string));
            tempDT.Columns.Add("OKToUpload", typeof(bool));

            try
            {
                foreach (KeyValuePair<string, string> sRegister in deserializeJsonObj)
                {
                    string regId = sRegister.Key;
                    if (!String.IsNullOrEmpty(regId))
                    {
                        string regActualVal = sRegister.Value;

                        if (!String.IsNullOrEmpty(regActualVal))
                        {
                            DataRow dr; double dTemp = 0;
                            dr = tempDT.NewRow();

                            RootObject regRecord = machinePropertiesList.Where(s => s.RegisterID == regId).Select(p => p).FirstOrDefault();
                            if (regRecord != null)
                            {

                                dr["PlcRunID"] = plcRunId;
                                dr["PLCRegisterID"] = regId;
                                dr["ParameterCategory"] = regRecord.Category;
                                //add sub-category after sql table 
                                dr["ParameterUnit"] = regRecord.pUOM;
                                dr["ParameterName"] = regRecord.pName;
                                dr["OKToUpload"] = true;

                                string pTypeValue = regRecord.pType;
                                string sBitOrCode = regRecord.pVal.Select(s => s.BitOrCode).FirstOrDefault();
                                string sVal = regRecord.pVal.Select(s => s.Val).FirstOrDefault();
                                string sUcl = regRecord.pVal.Select(s => s.UCL).FirstOrDefault();
                                string sLcl = regRecord.pVal.Select(s => s.LCL).FirstOrDefault();
                                string sOperator = regRecord.pVal.Select(s => s.OPR).FirstOrDefault();
                                string sFactor = regRecord.pVal.Select(s => s.Factor).FirstOrDefault();


                                switch (pTypeValue.ToUpper())
                                {
                                    case "STR":
                                        dr["ParameterValueActual"] = regActualVal;
                                        dr["ParameterValueDisplay"] = sVal;
                                        tempDT.Rows.Add(dr);
                                        break;
                                    case "INT":
                                        string valTemp = "Default#" + sVal;
                                        if (!String.IsNullOrEmpty(sUcl))
                                        {
                                            valTemp = valTemp + " UCL# " + sUcl;
                                        }
                                        if (!String.IsNullOrEmpty(sLcl))
                                        {
                                            valTemp = valTemp + " LCL# " + sLcl;
                                        }
                                        dr["ParameterValueDisplay"] = valTemp;

                                        if (!String.IsNullOrEmpty(regActualVal))
                                        {
                                            Decimal tempDActVal = Decimal.Parse(regActualVal, System.Globalization.NumberStyles.Float, CultureInfo.InvariantCulture);


                                            int iActVal = Convert.ToInt32(tempDActVal);
                                            //ID MasterMappingID RegisterProperties 
                                            //1   NULL    { "UNQID":"1234-1001-2001-41001","OEMID":"1234","OEMDesc":"Nichrome","MasterID":"1001","MasterDesc":"LiquidFilling","PlcID":"2001","PlcDesc":"LH","RegisterID":"41001","Category":"IDENTITY","Sub-Category":"MyValue","pName":"LOGGER ID","pUOM":"MACID",
                                            //"pType":"STR","pVal":[{"BitOrCode":"","Val":"00:1B:44:11:3A:B7","UCL":"","LCL":"","OPR":"","Factor":""}]}
                                            //2	NULL	{"UNQID":"1234-1001-2001-41002","OEMID":"1234","OEMDesc":"Nichrome","MasterID":"1001","MasterDesc":"LiquidFilling","PlcID":"2001","PlcDesc":"LH","RegisterID":"41002","Category":"IDENTITY","Sub-Category":"MyValue","pName":"OEM ID","pUOM":"unit",
                                            //"pType":"INT","pVal":[{"BitOrCode":"","Val":"1234","UCL":"","LCL":"","OPR":"","Factor":""}]}	2019-12-17 03:53:00	NULL NULL    NULL
                                            //5	NULL	{"UNQID":"1234-1001-2001-41005","OEMID":"1234","OEMDesc":"Nichrome","MasterID":"1001","MasterDesc":"LiquidFilling","PlcID":"2001","PlcDesc":"LH","RegisterID":"41005","Category":"Web Servo","Sub-Category":"MyValue","pName":"Temp","pUOM":"degC",
                                            //"pType":"FLT","pVal":[{"BitOrCode":"","Val":"50","UCL":"-30","LCL":"60","OPR":"/","Factor":"2"}]}	2019-12-17 03:53:00	NULL NULL    NULL


                                            if (!String.IsNullOrEmpty(sFactor) && (!String.IsNullOrEmpty(sOperator)))
                                            {

                                                // double dActVal = Convert.ToDouble(regActualVal);
                                                double dFactor = Convert.ToDouble(sFactor);
                                                if (dFactor != 0)
                                                {
                                                    switch (sOperator)
                                                    {
                                                        case "+":
                                                            dTemp = iActVal + dFactor;
                                                            break;
                                                        case "-":
                                                            dTemp = iActVal - dFactor;
                                                            break;
                                                        case "*":
                                                            dTemp = iActVal * dFactor;
                                                            break;
                                                        case "/":
                                                            dTemp = iActVal / dFactor;
                                                            break;
                                                        case "%":
                                                            dTemp = iActVal % dFactor;
                                                            break;
                                                        default:
                                                            dTemp = iActVal;
                                                            break;

                                                    }//switchCase
                                                    iActVal = Convert.ToInt32(dTemp);
                                                }// if (dFactor != 0)

                                            }// if (!String.IsNullOrEmpty(sFactor) && (!String.IsNullOrEmpty(sOperator)))

                                            dr["ParameterValueActual"] = String.Format("{0:0}", iActVal);

                                            if (!String.IsNullOrEmpty(sUcl))
                                            {
                                               if(iActVal> (Convert.ToInt32(sUcl)))
                                               {
                                                    log.Info("ALERT Actual Value > UCL "+recId+" Actual# "+iActVal+" UCL# "+sUcl);
                                               }
                                            }
                                            if (!String.IsNullOrEmpty(sLcl))
                                            {
                                                if (iActVal < (Convert.ToInt32(sLcl)))
                                                {
                                                    log.Info("ALERT Actual Value < LCL " + recId + " Actual# " + iActVal + " LCL# " + sLcl);
                                                }
                                            }
                                           

                                        }//if (!String.IsNullOrEmpty(regActualVal))
                                        else
                                        {
                                            dr["ParameterValueActual"] = regActualVal;
                                        }

                                        tempDT.Rows.Add(dr);
                                        break;
                                    case "FLT":

                                        valTemp = "Default#" + sVal;
                                        if (!String.IsNullOrEmpty(sUcl))
                                        {
                                            valTemp = valTemp + " UCL# " + sUcl;
                                        }
                                        if (!String.IsNullOrEmpty(sLcl))
                                        {
                                            valTemp = valTemp + " LCL# " + sLcl;
                                        }

                                        dr["ParameterValueDisplay"] = valTemp;

                                        if (!String.IsNullOrEmpty(regActualVal))
                                        {
                                            Decimal tempDActValFloat = Decimal.Parse(regActualVal, System.Globalization.NumberStyles.Float, CultureInfo.InvariantCulture);

                                            double dActValF = Convert.ToDouble(tempDActValFloat);

                                            if (!String.IsNullOrEmpty(sFactor) && (!String.IsNullOrEmpty(sOperator)))
                                            {

                                                double dFactor = Convert.ToDouble(sFactor);
                                                if (dFactor != 0)
                                                {
                                                    switch (sOperator)
                                                    {
                                                        case "+":
                                                            dTemp = dActValF + dFactor;
                                                            break;
                                                        case "-":
                                                            dTemp = dActValF - dFactor;
                                                            break;
                                                        case "*":
                                                            dTemp = dActValF * dFactor;
                                                            break;
                                                        case "/":
                                                            dTemp = dActValF / dFactor;
                                                            break;
                                                        case "%":
                                                            dTemp = dActValF % dFactor;
                                                            break;
                                                        default:
                                                            dTemp = dActValF;
                                                            break;

                                                    }//switch case
                                                    dActValF = dTemp;
                                                }// if (dFactor != 0)

                                            }// if (!String.IsNullOrEmpty(sFactor) && (!String.IsNullOrEmpty(sOperator)))

                                            dr["ParameterValueActual"] = String.Format("{0:0.00000000}", dActValF);

                                            if (!String.IsNullOrEmpty(sUcl))
                                            {
                                                if (dActValF > (Convert.ToDouble(sUcl)))
                                                {
                                                    log.Info("ALERT Actual Value > UCL " + recId + " Actual# " + String.Format("{0:0.00000000}", dActValF) + " UCL# " + sUcl);
                                                }
                                            }
                                            if (!String.IsNullOrEmpty(sLcl))
                                            {
                                                if (dActValF < (Convert.ToDouble(sLcl)))
                                                {
                                                    log.Info("ALERT Actual Value < LCL " + recId + " Actual# " + String.Format("{0:0.00000000}", dActValF) + " LCL# " + sLcl);
                                                }
                                            }

                                        }//if (!String.IsNullOrEmpty(regActualVal))
                                        else
                                        {
                                            dr["ParameterValueActual"] = regActualVal;
                                        }

                                        tempDT.Rows.Add(dr);
                                        break;

                                    case "CODE":
                                        //{..."pType":"CODE","pVal":[{"BitOrCode":"0","Val":"SCHNEIDER","UCL":"","LCL":"","OPR":"","Factor":""},{"BitOrCode":"1","Val":"DELTA","UCL":"","LCL":"","OPR":"","Factor":""},{},{}]}
                                        dr["BitCodePosition"] = regActualVal;
                                        var codeRec = regRecord.pVal.Where(s => s.BitOrCode == regActualVal).Select(p => p.Val).FirstOrDefault();
                                        if (codeRec != null)
                                            dr["ParameterValueActual"] = codeRec;
                                        else
                                            dr["OKToUpload"] = false;
                                        tempDT.Rows.Add(dr);
                                        break;
                                    case "BIT":
                                        //{ ...,"pType":"BIT","pVal":[{"BitOrCode":"0","Val":"POWER STATUS","UCL":"OFF","LCL":"ON","OPR":"","Factor":""},{"BitOrCode":"1","Val":"SWITCH","UCL":"OFF","LCL":"ON","OPR":"","Factor":""}]}
                                        string binary = Convert.ToString(Convert.ToInt32(regActualVal), 2);
                                        int size = binary.Length;
                                        char[] bitArray = binary.ToCharArray();
                                        Array.Reverse(bitArray);
                                        // { "RegData":{ "ID":41307,"pCat":"ON LINE ","pName":"CIP STATUS","pType":"BIT","pVal":[{"bPos":0,"bName":"REQUEST","b0":"NO","b1":"YES"},{"bPos":1,"bName":"PENDING","b0":"NO","b1":"YES"},{"bPos":2,"bName":"START","b0":"NO","b1":"YES"},{"bPos":3,"bName":"IN PROCESS","b0":"NO","b1":"YES"},{"bPos":4,"bName":"COMPLETE","b0":"NO","b1":"YES"}]}}
                                        //{"RegData":{"ID":41305,"pCat":"ON LINE ","pName":"MACHINE STATUS","pType":"BIT","pVal":[{"bPos":0,"bName":"POWER STATUS","b0":"OFF","b1":"ON"},{"bPos":1,"bName":"READY STATUS","b0":"OFF","b1":"ON"},{"bPos":2,"bName":"PRODUCTION STATUS","b0":"OFF","b1":"ON"}]}}
                                        for (int i = 0; i < bitArray.Length; i++)
                                        {
                                            if (i != 0)
                                            {
                                                dr = tempDT.NewRow();
                                                dr["PlcRunID"] = plcRunId;
                                                dr["PLCRegisterID"] = regId;
                                                dr["ParameterCategory"] = regRecord.Category;
                                                //add sub-category after sql table 
                                                dr["ParameterUnit"] = regRecord.pUOM;
                                                dr["ParameterName"] = regRecord.pName;
                                                dr["OKToUpload"] = true;
                                            }
                                            var bitRecord = regRecord.pVal.Where(s => Convert.ToInt32(s.BitOrCode) == i).FirstOrDefault();
                                            if (bitRecord == null)
                                            {
                                                dr["ParameterValueActual"] = "bit pos # " + i + " value not found ";
                                                dr["OKToUpload"] = false;
                                            }
                                            else if (bitArray[i] == '0')
                                            {
                                                dr["ParameterValueActual"] = bitRecord.Val + " " + bitRecord.UCL;
                                            }
                                            else
                                            {
                                                dr["ParameterValueActual"] = bitRecord.Val + " " + bitRecord.LCL;
                                            }
                                            dr["BitCodePosition"] = i;

                                            tempDT.Rows.Add(dr);
                                        }

                                        //foreach (char onebit in bitArray.Reverse())
                                        //{

                                        //    var bitRecord = regRecord.pVal.Where(s => Convert.ToInt32(s.BitOrCode) == bitPst).FirstOrDefault();
                                        //    if (bitRecord == null)
                                        //    {
                                        //        dr["ParameterValueActual"] = "bit pos # " + bitPst + " value not found ";
                                        //        dr["OKToUpload"] = false;
                                        //    }
                                        //    else if (onebit == '0')
                                        //    {
                                        //        dr["ParameterValueActual"] = bitRecord.Val + " " + bitRecord.UCL;
                                        //    }
                                        //    else
                                        //    {
                                        //        dr["ParameterValueActual"] = bitRecord.Val + " " + bitRecord.LCL;
                                        //    }
                                        //    tempDT.Rows.Add(dr);
                                        //    bitPst++;
                                        //    dr = tempDT.NewRow();
                                        //}
                                        //dr["ParameterName"] = "Bit Data Not Processed";
                                        break;

                                    default:

                                        break;
                                }

                                //  [PlcRunID],[PLCRegisterID],[BitCodePosition],[ParameterCategory],[ParameterUnit]
                                //  ,[ParameterName],[ParameterValueDisplay],[ParameterValueActual]

                                //@iPlcRunID,@sPLCRegisterID,@iBitCodePosition,@sParameterCategory,@sParameterUnit,@sParameterName,@sParameterValueDisplay,@sParameterValueActual)

                            }
                            else
                            {
                                dr["PlcRunID"] = plcRunId;
                                dr["PLCRegisterID"] = regId;
                                dr["ParameterName"] = "Master Data Not Found";
                                dr["ParameterValueActual"] = regActualVal;
                                dr["OKToUpload"] = false;
                                tempDT.Rows.Add(dr);
                            }
                        }
                        else
                        {
                            // regActualVal
                            log.Error("RegID# "+regId+" RegValue is Null");
                        }
                    }
                    else
                    {
                        //RegID
                        log.Error("RegId is Null");
                    }

                }
                string finalStr = string.Empty;
                if (tempDT.Rows.Count > 0)
                {
                    string tagInfoSerialize = iOTBL.CreateTagInfoNichrome(tempDT);

                    //function() tempdt

                    //Append logger record id and logger record creation time
                    DateTime dt = Convert.ToDateTime(recIDDT);
                    string rDateStr = dt.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    //string appendString = ",\"RID\":\"" + recId + "\", \"RDATE\":\"" + rDateStr + "\",\"STATUS\":\"OK\"}";

                    string appendString = ",\"RID\":\"" + recId + "\", \"RDATE\":\"" + rDateStr + "\"}";

                    finalStr = tagInfoSerialize.Replace("}", appendString);
               

                    var parameter = new SqlParameter("@tblcode", SqlDbType.Structured);
                    parameter.Value = tempDT;
                    parameter.TypeName = "dbo.tempDT";
                    //parameter.TypeName = "UserType";

                    using (var dbconn = new IoT_dataEntities1())
                    {
                        // dbconn.usp_Save_FinalData2();
                        int? rtvalue = dbconn.Database.ExecuteSqlCommand("exec dbo.usp_Save_FinalData @tblcode", parameter);
                        int? resVal = dbconn.usp_Set_plcRunID_TagInfo(finalStr, plcRunId);
                        result = true;
                    }
                }
                else
                {

                    finalStr = "{\"RID\":\"" + recId + "\", \"RDATE\":\"" + recIDDT + "\",\"STATUS\":\"ERROR\"}";
                    using (var dbconn = new IoT_dataEntities1())
                    {
                        int? resVal = dbconn.usp_Set_plcRunID_TagInfo(finalStr, plcRunId);
                        result = true;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                result = false;
            }
            return result;

        }


    }
}