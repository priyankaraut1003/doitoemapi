﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DOITOEMApi.Models
{
    public class MstRegisterData
    {
        public string registerId { get; set; }
        public string machineLineId { get; set; }
        public string PlcRegId { get; set; }
        public string PCat { get; set; }
        public string PUoc { get; set; }
        public string stringValue { get; set; }
        public string iVal { get; set; }
        public string iUOC { get; set; }
        public string iLcl { get; set; }
        public string fVal { get; set; }
        public string fUOC { get; set; }
        public string fLcl { get; set; }

    }
}