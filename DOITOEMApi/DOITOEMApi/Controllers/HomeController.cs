﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DOITOEMApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
           
            using (StreamReader r = new StreamReader(@"D:\Priyanka\WeightLoggeDocumets\SampleJsonData.json"))
            {
                var jsonFileData = r.ReadToEnd();
                string[] lines = jsonFileData.Split( new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
               
                InsertDataIntoJsonDT(lines);
            }

            return View();
        }

        private void InsertDataIntoJsonDT(string[] lines)
        {
            string connString = @"Data Source=diiotsqlserver1.database.windows.net;Initial Catalog=IoT_data;User ID=diappuser;password=di$1appuser";
           

            SqlConnection conn = new SqlConnection(connString);
            
            conn.Open();
            foreach (String item in lines)
            {
                string query = "Insert into IOT_JSON_DATA_V2(JsonData) values ('" + item + "')";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            
            conn.Close();

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}