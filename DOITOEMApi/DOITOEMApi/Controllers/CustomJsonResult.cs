﻿using System.Web.Mvc;

namespace DOITOEMApi.Controllers
{
    internal class CustomJsonResult
    {
        private object json;

        public CustomJsonResult(object json)
        {
            this.json = json;
        }

        public JsonRequestBehavior JsonRequestBehavior { get; set; }
    }
}