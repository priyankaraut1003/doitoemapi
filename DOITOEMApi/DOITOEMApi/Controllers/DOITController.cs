﻿
using DOITOEMApi.Models.Repository;
using Newtonsoft.Json.Linq;
using System;
using System.Data.SqlClient;
using System.Web.Http;

namespace DOITOEMApi.Controllers
{
    [RoutePrefix("api")]

    public class DOITController : ApiController
    {
        //#region variable Declaration
        IOTHubRepository _IOTHubRepository = new IOTHubRepository();
        //#endregion

        [HttpGet]
        //https://localhost:44316/api/IOTHub/ProcessLoggerData
        ////        [Authorize]
        [Route("IOTHub/ProcessLoggerData")]
        public JObject ReadDataFromIOTHub()
        {
           
            //Stopwatch sw;
            //sw = Stopwatch.StartNew();
            
           var resp= _IOTHubRepository.CheckLoggerData();
            return resp;
            //long executionTime = sw.ElapsedMilliseconds;

            // sw.Stop();

        }
        [HttpGet]
       
        ////        [Authorize]
        [Route("IOTHub/LiveToArchiveData")]
        public JObject LiveToArchiveData()
        {
            var response = _IOTHubRepository.LiveToArchiveData();
            return response;

        }
        //[HttpGet]
        //[Route("IOTHub/ReadCsvData")]
        //public void ReadCsvFile()
        //{
        //   // Stopwatch sw;
        //   // sw = Stopwatch.StartNew();
        //    String filePath = @"D:\Priyanka\WeightLoggeDocumets\NichromeNewMaster02JAN2020.csv";
        //    _IOTHubRepository.readCsvDataAndCreateJsonString(filePath);
        //    //long executionTime = sw.ElapsedMilliseconds;

        //    //sw.Stop();

        //}
        //[HttpGet]
        //[Route("IOTHub/SendLoggerData")]
        //public void SendLoggerDataToSqlTbl()
        //{
        //    SqlConnection thisConnection = new SqlConnection("data source=diiotsqlserver1.database.windows.net;Initial Catalog=IoT_data;Persist Security Info=True;User ID=diloggeruser;Password=Logger$1di;");

        //    try
        //    {
        //        SqlCommand thisCommand = thisConnection.CreateCommand();
        //        //add where clause =user MasterId PlcID

        //        thisConnection.Open();
        //        SqlCommand cmd = new SqlCommand("INSERT INTO IOT_JSON_DATA_V2(" + "JsonData1" + ") VALUES ('{\"dummy\":\"3\"}')", thisConnection);

        //        cmd.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        thisConnection.Close();
        //    }
        //}

        //public void ReadCsvFile([FromBody] String filePath)
        //{ 
        //    _IOTHubRepository.readCsvDataAndCreateJsonString(filePath);
        //}

    }
}
