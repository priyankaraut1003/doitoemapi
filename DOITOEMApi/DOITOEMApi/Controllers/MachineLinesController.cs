﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DOITOEMApi;

namespace DOITOEMApi.Controllers
{
    public class MachineLinesController : Controller
    {
        private IoT_dataEntities db = new IoT_dataEntities();

        // GET: MachineLines
        public ActionResult Index()
        {
            return View(db.mstMachineLines.ToList());
        }

        // GET: MachineLines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mstMachineLine mstMachineLine = db.mstMachineLines.Find(id);
            if (mstMachineLine == null)
            {
                return HttpNotFound();
            }
            return View(mstMachineLine);
        }

        // GET: MachineLines/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MachineLines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MachineLineID,MachineLineType,MaxRegisters,OEMID,IsActive")] mstMachineLine mstMachineLine)
        {
            if (ModelState.IsValid)
            {
                db.mstMachineLines.Add(mstMachineLine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mstMachineLine);
        }

        // GET: MachineLines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mstMachineLine mstMachineLine = db.mstMachineLines.Find(id);
            if (mstMachineLine == null)
            {
                return HttpNotFound();
            }
            return View(mstMachineLine);
        }

        // POST: MachineLines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MachineLineID,MachineLineType,MaxRegisters,OEMID,IsActive")] mstMachineLine mstMachineLine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mstMachineLine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mstMachineLine);
        }

        // GET: MachineLines/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mstMachineLine mstMachineLine = db.mstMachineLines.Find(id);
            if (mstMachineLine == null)
            {
                return HttpNotFound();
            }
            return View(mstMachineLine);
        }

        // POST: MachineLines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            mstMachineLine mstMachineLine = db.mstMachineLines.Find(id);
            db.mstMachineLines.Remove(mstMachineLine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
