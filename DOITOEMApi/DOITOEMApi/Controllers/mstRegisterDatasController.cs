﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DOITOEMApi;
using DOITOEMApi.Models;

namespace DOITOEMApi.Controllers
{
    public class mstRegisterDatasController : Controller
    {
        private IoT_dataEntities db = new IoT_dataEntities();

        // GET: mstRegisterDatas
        public ActionResult Index()
        {
            return View(db.mstRegisterDatas.ToList());
        }

        // GET: mstRegisterDatas/Details/5
        public ActionResult Details(int? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //mstRegisterData mstRegisterData = db.mstRegisterDatas.Find(id);
            //if (mstRegisterData == null)
            //{
            //    return HttpNotFound();
            //}
            return View();
        }

        // GET: mstRegisterDatas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: mstRegisterDatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,PlcRegisterID,MachineLineID,RegisterProperties,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy")] mstRegisterData mstRegisterData)
        {
            if (ModelState.IsValid)
            {
                db.mstRegisterDatas.Add(mstRegisterData);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mstRegisterData);
        }

        // GET: mstRegisterDatas/Edit/5
       
        // POST: mstRegisterDatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,PlcRegisterID,MachineLineID,RegisterProperties,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy")] mstRegisterData mstRegisterData)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mstRegisterData).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mstRegisterData);
        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult CallPartialView(string registerType)
        {
            string partialViewName = string.Empty;

            if (registerType == "1")
                partialViewName = "_StringRegisterData";
            else if (registerType == "2")
                partialViewName = "_IntRegiterData";
            else if (registerType == "3")
                partialViewName = "_FloatRegisterData";
            else if (registerType == "4")
                partialViewName = "_CodeRegisterData";
            else if (registerType == "5")
                partialViewName = "_BitRegisterData";

            return Json(partialViewName);
        }
        [HttpPost] 
        public JsonResult SaveDataStr(List<MstRegisterData> data)
        {
            var s = data;
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}
