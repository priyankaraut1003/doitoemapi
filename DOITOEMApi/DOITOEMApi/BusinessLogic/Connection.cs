﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Collections; 
using System.Text;
using System.Threading.Tasks;

namespace DOITOEMApi
{
    public class Connection
    {
        #region Member Variable


        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public static string SqlConnectionString
        {
            get
            {
                // return string.Empty;
                return ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;

            }
        }

       


        #endregion

        #region Constructor



        #endregion

        #region Member Function


        #endregion
    }
}