﻿using DOITOEMApi.Models;
using DOITOEMApi.Models.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ServiceStack.Messaging.Rcon;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

namespace DOITOEMApi.BusinessLogic
{
    public class IOTBL
    {
        IOTHubRepository iOTHubRepository;
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        

        public List<String> GetMstRegisterDataList(string machineLineid)
        {
            List<string> RootObjectList = new List<string>();
            try
            {
                using (var dbconn = new IoT_dataEntities1())
                {
                    var GetMstRegisterDataList = dbconn.usp_GetMstRegisterDataUsingUk(machineLineid);

                    foreach (var item in GetMstRegisterDataList.ToList())
                    {
                        String jsonString = item.RegisterProperties;
                        jsonString = "{\"UNQID\":\"1234-1001-2001-41001\",\"OEMID\":\"1234\",\"OEMDesc\":\"Nichrome\",\"MasterID\":\"1001\",\"MasterDesc\":\"LiquidFilling\",\"PlcID\":\"2001\",\"PlcDesc\":\"LH\",\"RegisterID\":\"41001\",\"Category\":\"IDENTITY\",\"pName\":\"LOGGER ID\",\"pUOM\":\"MACID\",\"pType\":\"STR\",\"pVal\":[{\"BitOrCode\":\"\",\"Val\":\"00:1B:44:11:3A:B7\",\"UCL\":\"\",\"LCL\":\"\",\"OPR\":\"\",\"Factor\":\"\"}]}";

                        RootObjectList.Add(jsonString);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }

            return RootObjectList;
        }

        public List<RootObject> usp_GetMstRegisterDataList(string machineLineid)
        {
            List<RootObject> RootObjectList = new List<RootObject>();
            try
            {
                using (var dbconn = new IoT_dataEntities1())
                {
                    var GetMstRegisterDataList = dbconn.usp_GetMstRegisterDataUsingUk(machineLineid);

                    foreach (var item in GetMstRegisterDataList.ToList())
                    {
                        String jsonString = item.RegisterProperties;
                        // jsonString = "{\"UNQID\":\"1234-1001-2001-41001\",\"OEMID\":\"1234\",\"OEMDesc\":\"Nichrome\",\"MasterID\":\"1001\",\"MasterDesc\":\"LiquidFilling\",\"PlcID\":\"2001\",\"PlcDesc\":\"LH\",\"RegisterID\":\"41001\",\"Category\":\"IDENTITY\",\"Sub-Category\":\"MyValue\",\"pName\":\"LOGGER ID\",\"pUOM\":\"MACID\",\"pType\":\"STR\",\"pVal\":[{\"BitOrCode\":\"\",\"Val\":\"00:1B:44:11:3A:B7\",\"UCL\":\"\",\"LCL\":\"\",\"OPR\":\"\",\"Factor\":\"\"}]}";

                        // JObject googleSearch = JObject.Parse(jsonString);

                        RootObject machineData = JsonConvert.DeserializeObject<RootObject>(jsonString);

                        RootObjectList.Add(machineData);

                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
            
            return RootObjectList;
        }
        public string CreateTagInfoNichrome(DataTable tempDT)
        {
            string json = string.Empty;
            string[] searchArray = {"OEMID","LOGGERID","SONO","YEAR","BAGGER","TYPE","FILLER","CUSTOMERNO","HEAD","PRODUCT CATEGORY", "BAG CLEAR TIME", "EMPTY BAG COUNT", "TOTAL FILLED BAG COUNT", "BAG LENGTH SETTING", "MACHINE SPEED", "TOTAL RUNNING HOURS", "STAND STILL HOURS", "QUANTITY", "MEASURE", "MOTOR SPEED (RPM)", "ACC/DCC" };

            //"BAG CLEAR TIME","EMPTY BAG COUNT","TOTAL FILLED BAG COUNT","BAG LENGTH SETTING","MACHINE SPEED","TOTAL RUNNING HOURS"
           // ----"STAND STILL HOURS","QUANTITY","MEASURE","MOTOR SPEED (RPM)","ACC/DCC"
            try
            {
                Dictionary<string, string> dct = new Dictionary<string, string>();
                for (int i = 0; i < searchArray.Length; i++)
                {
                    var filterData = tempDT.AsEnumerable().Where(s => s.Field<string>("ParameterName") == searchArray[i]).Select(P => P.Field<string>("ParameterValueActual")).FirstOrDefault();
                    if (filterData != null)
                        dct.Add(searchArray[i], filterData.ToString());
                }
                
                if (dct.ContainsKey("YEAR") && dct.ContainsKey("SONO"))
                {
                    string machineId = dct["YEAR"] + "-" + dct["SONO"];
                    dct.Add("MACHINEID", machineId);
                }
               
                if (dct.ContainsKey("CUSTOMERNO"))
                {
                    Int32 custNo = Convert.ToInt32(dct["CUSTOMERNO"]);
                    iOTHubRepository = new IOTHubRepository();
                    //search cust name from different db 

                    Dictionary<string,string> customerDct = iOTHubRepository.GetCustomerData(custNo);
                    if (customerDct.Count > 0)
                    {
                        foreach (var item in customerDct)
                        {
                            dct.Add(item.Key, item.Value);
                        }
                    }
                    else
                    {
                        dct.Add("CUSTOMER DETAILS", "NOT FOUND");
                    }
                }

                //implement Product Info
                if (dct.ContainsKey("PRODUCT CATEGORY"))
                {
                    Int32 productId = Convert.ToInt32(dct["PRODUCT CATEGORY"]);
                    Dictionary<string, string> productDct = iOTHubRepository.GetProductDetails(productId);
                    if (productDct.Count > 0)
                    {
                        foreach (var item in productDct)
                        {
                            dct.Add(item.Key, item.Value);
                        }
                    }
                    else
                    {
                        dct.Add("PRODUCT DETAILS", "NOT FOUND");
                    }
                }
                // Need to implement for get Instalation Date
                
                    //string instDate = dct["YEAR"] + "-" + dct["SONO"];
                    dct.Add("INSTDATE", "Dummy 2000-01-01");

                    //select DISTINCT SALESID, CustomerID, OEMID, InstallationDate ,itemId
                   // from Swas_CustomerMachinesList
                    //where CustomerID like '%116%'

                json = JsonConvert.SerializeObject(dct, Formatting.None);
            }catch(Exception ex)
            {
                log.Error(ex.Message);
            }
            return json;
        }
       
       
        public void createxml(DataTable jsonDT)
        {
            StringBuilder sb_UpdateAndInsertMstRegisterData = new StringBuilder();
            sb_UpdateAndInsertMstRegisterData.AppendLine("<UpdateAndInsertMasterRegisterProperties>");
            if (jsonDT.Rows.Count > 0)
            {
                for (int i = 0; i < jsonDT.Rows.Count; i++)
                {
                    sb_UpdateAndInsertMstRegisterData.AppendLine("<UpdateAndInsertMasterRegisterPropertiesData>");
                    sb_UpdateAndInsertMstRegisterData.AppendLine("<RegisterID>"+jsonDT.Rows[i]["RegisterID"] +"</RegisterID>");
                    sb_UpdateAndInsertMstRegisterData.AppendLine("<PlcID>" + jsonDT.Rows[i]["PlcID"] + "</PlcID>");
                    sb_UpdateAndInsertMstRegisterData.AppendLine("<RegisterProperties>" + jsonDT.Rows[i]["RegisterProperties"] + "</RegisterProperties>");
                    sb_UpdateAndInsertMstRegisterData.AppendLine("</UpdateAndInsertMasterRegisterPropertiesData>");
                }
                sb_UpdateAndInsertMstRegisterData.AppendLine("</UpdateAndInsertMasterRegisterProperties>");
                string xmlData = sb_UpdateAndInsertMstRegisterData.ToString().Replace("&", "&amp;").Replace(" <", "<").Replace("> ", ">");
                string strResult = string.Empty;
                Hashtable hInputPara = new Hashtable();
                string SynonymName = string.Empty;

                hInputPara.Add("@UpdateAndInsertXML", xmlData);
               // strResult = _OemKraKpi.UploadBudget_ExcelSheet(hInputPara);

            }

        }

        public DataTable ReadCsvFileUsingUniqueID(String FilePath)
        {
            DataTable dtCsv = new DataTable();
            string Fulltext = string.Empty;
            //string FileSaveWithPath = @"D:\Priyanka\WeightLoggeDocumets\sample-csv--register-dataUnSorted.csv";
                using (StreamReader sr = new StreamReader(FilePath))
                {
                    while (!sr.EndOfStream)
                    {
                        Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                        string[] rows = Fulltext.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                   // string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                            {
                                if (i == 0)
                                {
                                    dtCsv.Columns.Add("UNQID");
                                    for (int j = 0; j < rowValues.Count(); j++)
                                    {
                                        dtCsv.Columns.Add(rowValues[j]); //add headers  
                                    }
                                }
                                else
                                {
                                    DataRow dr = dtCsv.NewRow();
                                    for (int k = 0; k < rowValues.Count(); k++)
                                    {
                                        dr[k+1] = rowValues[k].ToString();
                                    }
                                //  dr["UNQID"] = dr[0].ToString() + dr[2].ToString()+dr[4].ToString() + dr[6].ToString();

                                dr["UNQID"] = dr[1].ToString() +"-"+ dr[3].ToString() +"-"+ dr[5].ToString() +"-"+ dr[7].ToString();
                                dtCsv.Rows.Add(dr); //add other rows  
                                }
                            }
                        }
                    }
                }
            
            return dtCsv;
        }

        public bool IsValidJson(string stringValue)
        {
            if (string.IsNullOrWhiteSpace(stringValue))
            {
                return false;
            }

            var value = stringValue.Trim();

            if ((value.StartsWith("{") && value.EndsWith("}")) || //For object
                (value.StartsWith("[") && value.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(value);
                    return true;
                }
                catch (JsonReaderException)
                {
                    log.Info("Error");
                    return false;
                }
            }

            return false;
        }

        public  JObject ValidateLoggerData(usp_GetIOTJsonDataByBatchNew_Result iotJsonData)
        {
            JObject jsonStrFinal = JObject.Parse(iotJsonData.JsonData1);

            if (!String.IsNullOrEmpty(iotJsonData.JsonData1))
            {
                jsonStrFinal = JObject.Parse(iotJsonData.JsonData1);

                if (!String.IsNullOrEmpty(iotJsonData.JsonData2))
                {
                    JObject jsonStrCol2 = JObject.Parse(iotJsonData.JsonData2);
                    jsonStrFinal.Merge(jsonStrCol2);

                }
                if (!String.IsNullOrEmpty(iotJsonData.JsonData3))
                {
                    JObject jsonStrCol3 = JObject.Parse(iotJsonData.JsonData2);
                    jsonStrFinal.Merge(jsonStrCol3);

                }
                if (!String.IsNullOrEmpty(iotJsonData.JsonData4))
                {
                    JObject jsonStrCol4 = JObject.Parse(iotJsonData.JsonData4);
                    jsonStrFinal.Merge(jsonStrCol4);
                }
                if (!String.IsNullOrEmpty(iotJsonData.JsonData5))
                {
                    JObject jsonStrCol5 = JObject.Parse(iotJsonData.JsonData5);
                    jsonStrFinal.Merge(jsonStrCol5);
                }
            }

            return jsonStrFinal;
        }

        public bool SaveDataIntoFinalDT(usp_GetIOTJsonDataByBatchNew_Result1 iotJsonData)
        {
            #region Declaration
            bool result = false;
            IOTBL iOTBL = new IOTBL();
            List<RootObject> machinePropertiesList;
            #endregion
            try
            {
                // log.Info("Log is StART");

                String jsonDataStr = iotJsonData.JsonData1;
                // String jsonDataStr = iotJsonData.Select(s => s).FirstOrDefault();
                var deserializeJsonObj = new JavaScriptSerializer().Deserialize<Dictionary<String, String>>(jsonDataStr);
                #region jsonString 1 to 10 reserve for Register 1 to 5 get information and 6 to 10 are spare Register Value
                //OEMID / MasterID / PlcID / MachineID / CustomerID / LoggerID / Spare /spare /Spare / spare

                var oemIDElement = deserializeJsonObj.ElementAt(0);
                if (!String.IsNullOrEmpty(oemIDElement.Value))
                {

                    var masterIdElement = deserializeJsonObj.ElementAt(1);
                    if (!String.IsNullOrEmpty(masterIdElement.Value))
                    {


                        var plcIdElement = deserializeJsonObj.ElementAt(2);
                        if (!String.IsNullOrEmpty(plcIdElement.Value))
                        {


                            //var loggerElement = deserializeJsonObj.ElementAt(0);
                            //var endCustomerIdElement = deserializeJsonObj.ElementAt(4);
                            //use for testing

                            string sUniqueKey = oemIDElement.Value + "-" + masterIdElement.Value + "-" + plcIdElement.Value + "%";  //"1234-1001-2001%";
                                                                                                                                    //string sUniqueKey= "1234-1001-2001%";
                            #endregion
                            machinePropertiesList = iOTBL.usp_GetMstRegisterDataList(sUniqueKey);

                            //After live data start use following line
                            //machinePropertiesList = iOTBL.usp_GetMstRegisterDataList(Convert.ToInt32( machinelineIdElement.Value));


                            if (machinePropertiesList != null && machinePropertiesList.Count > 1)
                            {
                                iOTHubRepository = new IOTHubRepository();
                                //machinePropertiesList is mstRegister Data List
                                //deserializeJsonObj is one record from IOT_JSON_DATA_V2(data sent by logger)
                                int plcRunID = iOTHubRepository.CreatePlcRunId(iotJsonData.ID);
                                if (plcRunID != 0 && plcRunID != null)
                                {
                                    //string dateFormat = "yyyy-MM-dd hh:mm:ss.fffffff tt";
                                    //string strDate = iotJsonData.CreatedDate.ToString();

                                    //DateTime jsonRecDate = DateTime.Parse(strDate);
                                    //string finalRecDt = jsonRecDate.ToString(dateFormat);

                                    result = iOTHubRepository.SaveDataIntoFinalDT(plcRunID, machinePropertiesList, deserializeJsonObj, iotJsonData.ID, iotJsonData.CreatedDate.ToString());
                                }

                            }
                            else
                            {
                                log.Error("Master Register Data is Not Valid");//json string not valid
                            }
                        }
                        else
                        {
                            log.Error("PlcID  is Null");
                        }

                    }
                    else
                    {
                        log.Error("MasterId is Null");
                    }
                }
                else
                {
                    log.Error("OEMID is Null");
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString());
                result = false;
                // throw;
            }

            return result;
        }



    }

}