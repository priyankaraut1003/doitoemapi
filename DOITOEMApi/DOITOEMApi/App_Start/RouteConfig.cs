﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DOITOEMApi
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                //defaults: new { controller = "DOIT", action = "ReadDataFromIOTHub", id = UrlParameter.Optional }
                defaults: new { controller = "DOIT", action = "ReadCsvData", id = UrlParameter.Optional }
               
            );
        }
    }
}
